# INTRODUCTION
------------

This module provides a Cookie Script Integration into your drupal site.

(https://cookie-script.com/)

# INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

# CONFIGURATION
-------------

Configure the Cookie Script Integration at (/admin/config/cookie_script).


# MAINTAINERS
-----------

Current maintainers:
 * João Mauricio (jmauricio)
 - (https://www.drupal.org/u/jmauricio)
